﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormationSNCF.Ressources
{
    /// <summary>
    /// Classe de gestion des saisies de valeurs numériques au travers de l'invite de commande
    /// </summary>
    public static class Numerique
    {
        /// <summary>
        /// Vérifie si une chaine saisie se compose uniquement de chiffres
        /// </summary>
        /// <param name="chaineSaisie">Une chaine de caractères</param>
        /// <returns>Vrai si la chaine de caractère est composée uniquement de chiffre, False dans le cas contraire</returns>
        public static bool IsNumerique(string chaineSaisie)
        {
            bool isNum = true;

            if (chaineSaisie.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < chaineSaisie.Length; i++)
            {
                if (!char.IsDigit(chaineSaisie, i))
                {
                    isNum = false;
                }
            }
            return isNum;
        }

        /// <summary>
        /// Prend en charge la saisie au clavier d'une valeur entière au travers de l'invite de commande
        /// </summary>
        /// <returns>la valeur entière saisie</returns>
        public static int SaisirEntier()
        {
            string valeurSaisie;
            do
            {
                Console.WriteLine("Saisir une valeur entière");
                valeurSaisie = Console.ReadLine();

            }
            while (!Numerique.IsNumerique(valeurSaisie));
            return int.Parse(valeurSaisie);
        }

        /// <summary>
        /// Prend en charge la saisie au clavier d'une valeur décimal au travers de l'invite de commande
        /// </summary>
        /// <returns>la valeur décimale saisie</returns>
        public static decimal SaisirDecimal()
        {
            string valeurSaisie;
            do
            {
                Console.WriteLine("Saisir une valeur decimale");
                valeurSaisie = Console.ReadLine();
                valeurSaisie = valeurSaisie.Replace('.', ',');

            }
            while (!Numerique.IsDecimal(valeurSaisie));
            return decimal.Parse(valeurSaisie);
        }

        /// <summary>
        /// Prend en charge la saisie au clavier d'un entier court au travers de l'invite de commande
        /// </summary>
        /// <returns>Un entier court saisie</returns>
        public static short SaisieEntierCourt()
        {
            string valeurSaisie;
            do
            {
                Console.WriteLine("Saisir une valeur entière");
                valeurSaisie = Console.ReadLine();

            }
            while (!Numerique.IsNumerique(valeurSaisie));
            return short.Parse(valeurSaisie);
        }

        /// <summary>
        /// Vérifie si une chaine saisie se compose uniquement de chiffres et éventuellement d'une virgule
        /// </summary>
        /// <param name="chaineSaisie">Une chaine de caractères</param>
        /// <returns>Vrai si la chaine de caractère est composée uniquement de chiffre et éventuellement d'une virgule, False dans le cas contraire</returns>
        public static bool IsDecimal(string chaineSaisie)
        {
            bool isNum = true;


            if (chaineSaisie.Length == 0 || !Plus_D_Un(chaineSaisie, ','))
            {
                return false;
            }

            for (int i = 0; i < chaineSaisie.Length; i++)
            {
                if (!char.IsDigit(chaineSaisie, i) && chaineSaisie[i] != ',')
                {
                    isNum = false;
                    Console.WriteLine("le caractère a la position " + (i + 1) + " n'est pas un chiffre");
                }
            }
            return isNum;

        }

        /// <summary>
        /// Vérifie si une chaine comporte plus d'un caractère spécifié
        /// </summary>
        /// <param name="chaine">Une chaine de caractère</param>
        /// <param name="caractere">Le caractère spécifié</param>
        /// <returns>Vrai si le caractère spécifié apparait plus d'une fois dans la chaine de caractères, Faux dans le cas contraire</returns>
        public static bool Plus_D_Un(string chaine, char caractere)
        {
            return chaine.IndexOf(caractere) == chaine.LastIndexOf(caractere);
        }
    }
}
