﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FormationSNCF.Modele
{
    [Serializable]
    /// <summary>
    /// Classe métier : représente une activité
    /// </summary>
    public class Activite
    {
        private string _libelleActivite;
        private List<ActionFormation> _lesActionsFormation;

        /// <summary>
        /// Accéder en lecture à l'ensemble des action de formation de l'activité
        /// </summary>
        public ReadOnlyCollection<ActionFormation> ActionFormation
        {
            get
            {
                return new ReadOnlyCollection<Modele.ActionFormation>(_lesActionsFormation);
            }
        }

        /// <summary>
        /// Obtient le libellé de l'activité
        /// </summary>
        public string LibelleActivite
        {
            get { return _libelleActivite; }
        }
        
        /// <summary>
        /// Initialise une activité
        /// </summary>
        /// <param name="libelleActivite">Le libellé de l'activité</param>
        public Activite(string libelleActivite)
        {
            _libelleActivite = libelleActivite;
            _lesActionsFormation = new List<ActionFormation>();
        }

        /// <summary>
        /// Initialiser une fonction de formation
        /// </summary>
        /// <param name="unCodeAction">Le code de l'action</param>
        /// <param name="unIntituleDeFormation">L'intituler de la formation</param>
        /// <param name="unCout">Le cout d'une formation</param>
        /// <param name="uneDuree">La durée de la formation</param>
        /// <param name="uneDate">La date de la formation</param>
        public void AjouterActionFormation(string unCodeAction, string unIntituleDeFormation, decimal unCout, int uneDuree, DateTime uneDate)
        {
            ActionFormation uneActionDeFormation = new ActionFormation(unCodeAction, unIntituleDeFormation, unCout, uneDuree, uneDate);
            _lesActionsFormation.Add(uneActionDeFormation);
        }

        /// <summary>
        /// Rechercher une action de formation
        /// </summary>
        /// <param name="unCodeAction">Code de l'action</param>
        /// <returns>actionFormation</returns>
        public ActionFormation ObtenirActionFormation(string unCodeAction)
        {
            ActionFormation actionFormation = null;
            foreach(ActionFormation actionFormationCourante in _lesActionsFormation)
            {
                if (actionFormationCourante.CodeAction == unCodeAction)
                {
                    actionFormation = actionFormationCourante;
                    break;
                }
            }
            return actionFormation;
        }

        /// <summary>
        /// Supprimer une action de formation
        /// </summary>
        /// <param name="codeAction">code de l'action</param>
        /// <returns>la collection des actions de formation</returns>
        public bool SupprimerActionFormation(string codeAction)
        {
            return _lesActionsFormation.Remove(ObtenirActionFormation(codeAction));
        }

        /// <summary>
        /// Verifier l'egalité entre l'bjet et l'objet transmis en paramètre
        /// </summary>
        /// <param name="uneActivite">une Activité</param>
        /// <returns>egal</returns>
        public override bool Equals(object uneActivite)
        {
            bool egal = false;
            Activite activiteCompare = uneActivite as Activite;
            if (activiteCompare != null)
            {
                if(activiteCompare.LibelleActivite == _libelleActivite)
                {
                    egal = true;
                }
            }

            return egal;
        }

        /// <summary>
        /// Rechercher dans un ensemble d'occurence de même type 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Retourner le libelle de l'activité 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
                return _libelleActivite;
        }


    }
}
