﻿namespace FormationSNCF.Vues
{
    partial class FormGestionActionFormation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBoxListeActivite = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxIntituleActionFormation = new System.Windows.Forms.TextBox();
            this.buttonAjoutActionFormation = new System.Windows.Forms.Button();
            this.textBoxCoutAction = new System.Windows.Forms.TextBox();
            this.textBoxDureeAction = new System.Windows.Forms.TextBox();
            this.textBoxDateCreation = new System.Windows.Forms.TextBox();
            this.textBoxCodeAction = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridViewActionDeFormation = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActionDeFormation)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxListeActivite
            // 
            this.comboBoxListeActivite.FormattingEnabled = true;
            this.comboBoxListeActivite.ItemHeight = 15;
            this.comboBoxListeActivite.Location = new System.Drawing.Point(295, 35);
            this.comboBoxListeActivite.Margin = new System.Windows.Forms.Padding(8);
            this.comboBoxListeActivite.Name = "comboBoxListeActivite";
            this.comboBoxListeActivite.Size = new System.Drawing.Size(230, 23);
            this.comboBoxListeActivite.TabIndex = 1;
            this.comboBoxListeActivite.SelectedIndexChanged += new System.EventHandler(this.ComboxListeActivite_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sélectionnez une activité";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxIntituleActionFormation);
            this.groupBox1.Controls.Add(this.buttonAjoutActionFormation);
            this.groupBox1.Controls.Add(this.textBoxCoutAction);
            this.groupBox1.Controls.Add(this.textBoxDureeAction);
            this.groupBox1.Controls.Add(this.textBoxDateCreation);
            this.groupBox1.Controls.Add(this.textBoxCodeAction);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(30, 120);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(887, 141);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action de l\'activité";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(63, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 15);
            this.label6.TabIndex = 10;
            this.label6.Text = "Intitulé de l\'action";
            // 
            // textBoxIntituleActionFormation
            // 
            this.textBoxIntituleActionFormation.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxIntituleActionFormation.Location = new System.Drawing.Point(358, 23);
            this.textBoxIntituleActionFormation.Name = "textBoxIntituleActionFormation";
            this.textBoxIntituleActionFormation.Size = new System.Drawing.Size(506, 23);
            this.textBoxIntituleActionFormation.TabIndex = 2;
            // 
            // buttonAjoutActionFormation
            // 
            this.buttonAjoutActionFormation.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjoutActionFormation.Location = new System.Drawing.Point(572, 86);
            this.buttonAjoutActionFormation.Name = "buttonAjoutActionFormation";
            this.buttonAjoutActionFormation.Size = new System.Drawing.Size(292, 29);
            this.buttonAjoutActionFormation.TabIndex = 7;
            this.buttonAjoutActionFormation.Text = "Ajout d\'une action de formation";
            this.buttonAjoutActionFormation.UseVisualStyleBackColor = true;
            this.buttonAjoutActionFormation.Click += new System.EventHandler(this.ButtonAjoutActionFormation_Click);
            // 
            // textBoxCoutAction
            // 
            this.textBoxCoutAction.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCoutAction.Location = new System.Drawing.Point(427, 88);
            this.textBoxCoutAction.Name = "textBoxCoutAction";
            this.textBoxCoutAction.Size = new System.Drawing.Size(125, 23);
            this.textBoxCoutAction.TabIndex = 6;
            // 
            // textBoxDureeAction
            // 
            this.textBoxDureeAction.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDureeAction.Location = new System.Drawing.Point(317, 88);
            this.textBoxDureeAction.Name = "textBoxDureeAction";
            this.textBoxDureeAction.Size = new System.Drawing.Size(83, 23);
            this.textBoxDureeAction.TabIndex = 5;
            this.textBoxDureeAction.Leave += new System.EventHandler(this.TextBoxDureeAction_Leave);
            // 
            // textBoxDateCreation
            // 
            this.textBoxDateCreation.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDateCreation.Location = new System.Drawing.Point(191, 88);
            this.textBoxDateCreation.Name = "textBoxDateCreation";
            this.textBoxDateCreation.Size = new System.Drawing.Size(98, 23);
            this.textBoxDateCreation.TabIndex = 4;
            this.textBoxDateCreation.Leave += new System.EventHandler(this.TextBoxDateCreation_Leave);
            // 
            // textBoxCodeAction
            // 
            this.textBoxCodeAction.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCodeAction.Location = new System.Drawing.Point(67, 88);
            this.textBoxCodeAction.Name = "textBoxCodeAction";
            this.textBoxCodeAction.Size = new System.Drawing.Size(83, 23);
            this.textBoxCodeAction.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(423, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Coût";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(313, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Durée";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(191, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(83, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Code";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridViewActionDeFormation);
            this.groupBox2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(30, 282);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(887, 396);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Liste des actions de formations de l\'activité sélectionnée";
            // 
            // dataGridViewActionDeFormation
            // 
            this.dataGridViewActionDeFormation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewActionDeFormation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewActionDeFormation.Location = new System.Drawing.Point(31, 43);
            this.dataGridViewActionDeFormation.Name = "dataGridViewActionDeFormation";
            this.dataGridViewActionDeFormation.RowTemplate.Height = 24;
            this.dataGridViewActionDeFormation.Size = new System.Drawing.Size(821, 322);
            this.dataGridViewActionDeFormation.TabIndex = 17;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.comboBoxListeActivite);
            this.groupBox3.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(30, 23);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(887, 80);
            this.groupBox3.TabIndex = 16;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Liste des activités";
            // 
            // FormGestionActionFormation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(962, 711);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1040, 756);
            this.MinimizeBox = false;
            this.Name = "FormGestionActionFormation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION DES ACTIONS DE FORMATIONS";
            this.Load += new System.EventHandler(this.FormGestionActionFormation_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActionDeFormation)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxListeActivite;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxCoutAction;
        private System.Windows.Forms.TextBox textBoxDureeAction;
        private System.Windows.Forms.TextBox textBoxDateCreation;
        private System.Windows.Forms.TextBox textBoxCodeAction;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonAjoutActionFormation;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxIntituleActionFormation;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGridViewActionDeFormation;
    }
}