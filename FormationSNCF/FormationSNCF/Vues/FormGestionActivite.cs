﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionActivite : Form
    {
        public FormGestionActivite()
        {
            InitializeComponent();
        }

        private void FormAjoutActivite_Load(object sender, EventArgs e)
        {
            listBoxListeActivites.DataSource = Donnees.CollectionActivite;
        }

        private void ButtonAjoutActivite_Click(object sender, EventArgs e)
        {
            if (textBoxNomActivite.Text.Length < 3)
            {
                MessageBox.Show("L'activite doit etre compose d'au moins 3 caracteres");
                textBoxNomActivite.Text = "";
                textBoxNomActivite.Focus();
            }

            Activite uneActivite = new Activite(textBoxNomActivite.Text);
            Donnees.CollectionActivite.Add(uneActivite);
            listBoxListeActivites.DataSource = Donnees.CollectionActivite;

        }
    }
}
