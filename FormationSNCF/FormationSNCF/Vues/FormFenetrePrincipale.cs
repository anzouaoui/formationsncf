﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormFenetrePrincipale : Form
    {
        private Form _mdiChild;
        /// <summary>
        /// Acceder à la seule fentetre fille contenu dans la fenetre proncipale
        /// </summary>
        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if(_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }

        private void GestionDesLieuxToolStripMenuItem(object sender, EventArgs e)
        {

        }

        private void FormFenetrePrincipale_Closing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }

        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionLieu();
        }

        private void AjouterAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormAjouterAgent();
        }

        private void GestionDesActionsDeFormationsToolsTripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionActionFormation();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void GestionDesActivitesToolsTripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionActivite();
        }

        private void AfficherAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormListeAgent();
        }

        private void GestionDesSessionsDeFormationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionSessionDeFormation();
        }
    }
}
