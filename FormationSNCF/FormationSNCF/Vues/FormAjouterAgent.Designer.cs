﻿namespace FormationSNCF.Vues
{
    partial class FormAjouterAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ajoutAgentGroupBox = new System.Windows.Forms.GroupBox();
            this.labelNumeroAgent = new System.Windows.Forms.Label();
            this.ButtonAjoutAgent = new System.Windows.Forms.Button();
            this.comboBoxLieu = new System.Windows.Forms.ComboBox();
            this.comboBoxCivilite = new System.Windows.Forms.ComboBox();
            this.textBoxDateEmbauche = new System.Windows.Forms.TextBox();
            this.textBoxDateNaissance = new System.Windows.Forms.TextBox();
            this.textBoxVille = new System.Windows.Forms.TextBox();
            this.textBoxCodePostal = new System.Windows.Forms.TextBox();
            this.textBoxAdresse2 = new System.Windows.Forms.TextBox();
            this.textBoxAdresse1 = new System.Windows.Forms.TextBox();
            this.textBoxPrenom = new System.Windows.Forms.TextBox();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ajoutAgentGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // ajoutAgentGroupBox
            // 
            this.ajoutAgentGroupBox.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ajoutAgentGroupBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ajoutAgentGroupBox.Controls.Add(this.labelNumeroAgent);
            this.ajoutAgentGroupBox.Controls.Add(this.ButtonAjoutAgent);
            this.ajoutAgentGroupBox.Controls.Add(this.comboBoxLieu);
            this.ajoutAgentGroupBox.Controls.Add(this.comboBoxCivilite);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxDateEmbauche);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxDateNaissance);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxVille);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxCodePostal);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxAdresse2);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxAdresse1);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxPrenom);
            this.ajoutAgentGroupBox.Controls.Add(this.textBoxNom);
            this.ajoutAgentGroupBox.Controls.Add(this.label11);
            this.ajoutAgentGroupBox.Controls.Add(this.label10);
            this.ajoutAgentGroupBox.Controls.Add(this.label9);
            this.ajoutAgentGroupBox.Controls.Add(this.label8);
            this.ajoutAgentGroupBox.Controls.Add(this.label7);
            this.ajoutAgentGroupBox.Controls.Add(this.label6);
            this.ajoutAgentGroupBox.Controls.Add(this.label5);
            this.ajoutAgentGroupBox.Controls.Add(this.label4);
            this.ajoutAgentGroupBox.Controls.Add(this.label3);
            this.ajoutAgentGroupBox.Controls.Add(this.label2);
            this.ajoutAgentGroupBox.Controls.Add(this.label1);
            this.ajoutAgentGroupBox.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ajoutAgentGroupBox.Location = new System.Drawing.Point(16, 18);
            this.ajoutAgentGroupBox.Margin = new System.Windows.Forms.Padding(5);
            this.ajoutAgentGroupBox.Name = "ajoutAgentGroupBox";
            this.ajoutAgentGroupBox.Padding = new System.Windows.Forms.Padding(5);
            this.ajoutAgentGroupBox.Size = new System.Drawing.Size(968, 337);
            this.ajoutAgentGroupBox.TabIndex = 0;
            this.ajoutAgentGroupBox.TabStop = false;
            // 
            // labelNumeroAgent
            // 
            this.labelNumeroAgent.AutoSize = true;
            this.labelNumeroAgent.BackColor = System.Drawing.Color.LightSkyBlue;
            this.labelNumeroAgent.Font = new System.Drawing.Font("Lucida Bright", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumeroAgent.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelNumeroAgent.Location = new System.Drawing.Point(203, 42);
            this.labelNumeroAgent.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelNumeroAgent.MinimumSize = new System.Drawing.Size(230, 25);
            this.labelNumeroAgent.Name = "labelNumeroAgent";
            this.labelNumeroAgent.Size = new System.Drawing.Size(230, 25);
            this.labelNumeroAgent.TabIndex = 24;
            // 
            // ButtonAjoutAgent
            // 
            this.ButtonAjoutAgent.Location = new System.Drawing.Point(764, 283);
            this.ButtonAjoutAgent.Name = "ButtonAjoutAgent";
            this.ButtonAjoutAgent.Size = new System.Drawing.Size(127, 25);
            this.ButtonAjoutAgent.TabIndex = 23;
            this.ButtonAjoutAgent.Text = "AJOUTER";
            this.ButtonAjoutAgent.UseVisualStyleBackColor = true;
            this.ButtonAjoutAgent.Click += new System.EventHandler(this.ButtonAjoutAgent_Click);
            // 
            // comboBoxLieu
            // 
            this.comboBoxLieu.FormattingEnabled = true;
            this.comboBoxLieu.ItemHeight = 15;
            this.comboBoxLieu.Location = new System.Drawing.Point(697, 228);
            this.comboBoxLieu.Name = "comboBoxLieu";
            this.comboBoxLieu.Size = new System.Drawing.Size(230, 23);
            this.comboBoxLieu.TabIndex = 11;
            // 
            // comboBoxCivilite
            // 
            this.comboBoxCivilite.FormattingEnabled = true;
            this.comboBoxCivilite.ItemHeight = 15;
            this.comboBoxCivilite.Items.AddRange(new object[] {
            "Sélectionner",
            "M.",
            "Mme.",
            "Mlle."});
            this.comboBoxCivilite.Location = new System.Drawing.Point(202, 79);
            this.comboBoxCivilite.Name = "comboBoxCivilite";
            this.comboBoxCivilite.Size = new System.Drawing.Size(230, 23);
            this.comboBoxCivilite.TabIndex = 2;
            // 
            // textBoxDateEmbauche
            // 
            this.textBoxDateEmbauche.Location = new System.Drawing.Point(203, 231);
            this.textBoxDateEmbauche.Name = "textBoxDateEmbauche";
            this.textBoxDateEmbauche.Size = new System.Drawing.Size(230, 23);
            this.textBoxDateEmbauche.TabIndex = 10;
            this.textBoxDateEmbauche.Leave += new System.EventHandler(this.TextBoxDateEmbauche_Leave);
            // 
            // textBoxDateNaissance
            // 
            this.textBoxDateNaissance.Location = new System.Drawing.Point(203, 201);
            this.textBoxDateNaissance.Name = "textBoxDateNaissance";
            this.textBoxDateNaissance.Size = new System.Drawing.Size(230, 23);
            this.textBoxDateNaissance.TabIndex = 9;
            this.textBoxDateNaissance.Leave += new System.EventHandler(this.TextBoxDateNaissance_Leave);
            // 
            // textBoxVille
            // 
            this.textBoxVille.Location = new System.Drawing.Point(697, 177);
            this.textBoxVille.Name = "textBoxVille";
            this.textBoxVille.Size = new System.Drawing.Size(230, 23);
            this.textBoxVille.TabIndex = 8;
            // 
            // textBoxCodePostal
            // 
            this.textBoxCodePostal.Location = new System.Drawing.Point(697, 131);
            this.textBoxCodePostal.Name = "textBoxCodePostal";
            this.textBoxCodePostal.Size = new System.Drawing.Size(230, 23);
            this.textBoxCodePostal.TabIndex = 7;
            this.textBoxCodePostal.Leave += new System.EventHandler(this.TextBoxCodePostal_Leave);
            // 
            // textBoxAdresse2
            // 
            this.textBoxAdresse2.Location = new System.Drawing.Point(697, 85);
            this.textBoxAdresse2.Name = "textBoxAdresse2";
            this.textBoxAdresse2.Size = new System.Drawing.Size(230, 23);
            this.textBoxAdresse2.TabIndex = 6;
            // 
            // textBoxAdresse1
            // 
            this.textBoxAdresse1.Location = new System.Drawing.Point(697, 39);
            this.textBoxAdresse1.Name = "textBoxAdresse1";
            this.textBoxAdresse1.Size = new System.Drawing.Size(230, 23);
            this.textBoxAdresse1.TabIndex = 5;
            // 
            // textBoxPrenom
            // 
            this.textBoxPrenom.Location = new System.Drawing.Point(203, 137);
            this.textBoxPrenom.Name = "textBoxPrenom";
            this.textBoxPrenom.Size = new System.Drawing.Size(230, 23);
            this.textBoxPrenom.TabIndex = 4;
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(202, 107);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(230, 23);
            this.textBoxNom.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label11.Location = new System.Drawing.Point(494, 234);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(136, 15);
            this.label11.TabIndex = 10;
            this.label11.Text = "Lieu de ratachement";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label10.Location = new System.Drawing.Point(36, 231);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 15);
            this.label10.TabIndex = 9;
            this.label10.Text = "Date D\'embauche";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label9.Location = new System.Drawing.Point(36, 196);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 15);
            this.label9.TabIndex = 8;
            this.label9.Text = "Date de naissance";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label8.Location = new System.Drawing.Point(494, 180);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 7;
            this.label8.Text = "Ville";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label7.Location = new System.Drawing.Point(494, 134);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 6;
            this.label7.Text = "Code Postal";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label6.Location = new System.Drawing.Point(494, 88);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "Adresse 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label5.Location = new System.Drawing.Point(494, 42);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Adresse 1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label4.Location = new System.Drawing.Point(36, 132);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Prénom";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label3.Location = new System.Drawing.Point(35, 102);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label2.Location = new System.Drawing.Point(35, 72);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Civilité";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.label1.Location = new System.Drawing.Point(35, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero Agent";
            // 
            // FormAjouterAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1008, 388);
            this.Controls.Add(this.ajoutAgentGroupBox);
            this.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAjouterAgent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FORMULAIRE AJOUT AGENT";
            this.Load += new System.EventHandler(this.FormAjoutAgent_Load);
            this.ajoutAgentGroupBox.ResumeLayout(false);
            this.ajoutAgentGroupBox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox ajoutAgentGroupBox;
        private System.Windows.Forms.ComboBox comboBoxLieu;
        private System.Windows.Forms.ComboBox comboBoxCivilite;
        private System.Windows.Forms.TextBox textBoxDateEmbauche;
        private System.Windows.Forms.TextBox textBoxDateNaissance;
        private System.Windows.Forms.TextBox textBoxVille;
        private System.Windows.Forms.TextBox textBoxCodePostal;
        private System.Windows.Forms.TextBox textBoxAdresse2;
        private System.Windows.Forms.TextBox textBoxAdresse1;
        private System.Windows.Forms.TextBox textBoxPrenom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ButtonAjoutAgent;
        private System.Windows.Forms.Label labelNumeroAgent;
    }
}