﻿namespace FormationSNCF.Vues
{
    partial class FormGestionSessionDeFormation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxDureeAction = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxCoutAction = new System.Windows.Forms.TextBox();
            this.textBoxDateCreationAction = new System.Windows.Forms.TextBox();
            this.textBoxCodeAction = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxListeAction = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxListeLieu = new System.Windows.Forms.ComboBox();
            this.buttonAjouterSession = new System.Windows.Forms.Button();
            this.textBoxCoutHebergementSession = new System.Windows.Forms.TextBox();
            this.textBoxNbParticipantSession = new System.Windows.Forms.TextBox();
            this.textBoxDateSession = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxListeActivite = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridViewSessionsDeFormation = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSessionsDeFormation)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxDureeAction
            // 
            this.textBoxDureeAction.Location = new System.Drawing.Point(114, 122);
            this.textBoxDureeAction.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDureeAction.Name = "textBoxDureeAction";
            this.textBoxDureeAction.Size = new System.Drawing.Size(136, 23);
            this.textBoxDureeAction.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxCoutAction);
            this.groupBox1.Controls.Add(this.textBoxDureeAction);
            this.groupBox1.Controls.Add(this.textBoxDateCreationAction);
            this.groupBox1.Controls.Add(this.textBoxCodeAction);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBoxListeAction);
            this.groupBox1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(38, 117);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(1054, 169);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Action de l\'activité";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(38, 23);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(247, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "Sélectionnez une action de formation";
            // 
            // textBoxCoutAction
            // 
            this.textBoxCoutAction.Location = new System.Drawing.Point(463, 114);
            this.textBoxCoutAction.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxCoutAction.Name = "textBoxCoutAction";
            this.textBoxCoutAction.Size = new System.Drawing.Size(136, 23);
            this.textBoxCoutAction.TabIndex = 8;
            // 
            // textBoxDateCreationAction
            // 
            this.textBoxDateCreationAction.Location = new System.Drawing.Point(463, 77);
            this.textBoxDateCreationAction.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDateCreationAction.Name = "textBoxDateCreationAction";
            this.textBoxDateCreationAction.Size = new System.Drawing.Size(136, 23);
            this.textBoxDateCreationAction.TabIndex = 6;
            // 
            // textBoxCodeAction
            // 
            this.textBoxCodeAction.Location = new System.Drawing.Point(114, 77);
            this.textBoxCodeAction.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxCodeAction.Name = "textBoxCodeAction";
            this.textBoxCodeAction.Size = new System.Drawing.Size(136, 23);
            this.textBoxCodeAction.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(305, 122);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Coût";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 122);
            this.label4.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "Durée";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(305, 80);
            this.label3.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Date de création";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(38, 80);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Code";
            // 
            // comboBoxListeAction
            // 
            this.comboBoxListeAction.FormattingEnabled = true;
            this.comboBoxListeAction.ItemHeight = 15;
            this.comboBoxListeAction.Location = new System.Drawing.Point(459, 23);
            this.comboBoxListeAction.Margin = new System.Windows.Forms.Padding(5);
            this.comboBoxListeAction.Name = "comboBoxListeAction";
            this.comboBoxListeAction.Size = new System.Drawing.Size(304, 23);
            this.comboBoxListeAction.TabIndex = 0;
            this.comboBoxListeAction.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListeAction_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(43, 34);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 15);
            this.label7.TabIndex = 1;
            this.label7.Text = "Date";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(38, 86);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(93, 15);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nb Participant";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxListeLieu);
            this.groupBox2.Controls.Add(this.buttonAjouterSession);
            this.groupBox2.Controls.Add(this.textBoxCoutHebergementSession);
            this.groupBox2.Controls.Add(this.textBoxNbParticipantSession);
            this.groupBox2.Controls.Add(this.textBoxDateSession);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(38, 305);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox2.Size = new System.Drawing.Size(1054, 167);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Session de l\'action de formation";
            // 
            // comboBoxListeLieu
            // 
            this.comboBoxListeLieu.FormattingEnabled = true;
            this.comboBoxListeLieu.ItemHeight = 15;
            this.comboBoxListeLieu.Location = new System.Drawing.Point(459, 30);
            this.comboBoxListeLieu.Margin = new System.Windows.Forms.Padding(5);
            this.comboBoxListeLieu.Name = "comboBoxListeLieu";
            this.comboBoxListeLieu.Size = new System.Drawing.Size(230, 23);
            this.comboBoxListeLieu.TabIndex = 11;
            // 
            // buttonAjouterSession
            // 
            this.buttonAjouterSession.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAjouterSession.Location = new System.Drawing.Point(459, 126);
            this.buttonAjouterSession.Margin = new System.Windows.Forms.Padding(5);
            this.buttonAjouterSession.Name = "buttonAjouterSession";
            this.buttonAjouterSession.Size = new System.Drawing.Size(230, 31);
            this.buttonAjouterSession.TabIndex = 10;
            this.buttonAjouterSession.Text = "Ajouter une session";
            this.buttonAjouterSession.UseVisualStyleBackColor = true;
            this.buttonAjouterSession.Click += new System.EventHandler(this.ButtonAjouterSession_Click);
            // 
            // textBoxCoutHebergementSession
            // 
            this.textBoxCoutHebergementSession.Location = new System.Drawing.Point(459, 79);
            this.textBoxCoutHebergementSession.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxCoutHebergementSession.Name = "textBoxCoutHebergementSession";
            this.textBoxCoutHebergementSession.Size = new System.Drawing.Size(230, 23);
            this.textBoxCoutHebergementSession.TabIndex = 9;
            // 
            // textBoxNbParticipantSession
            // 
            this.textBoxNbParticipantSession.Location = new System.Drawing.Point(190, 83);
            this.textBoxNbParticipantSession.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxNbParticipantSession.Name = "textBoxNbParticipantSession";
            this.textBoxNbParticipantSession.Size = new System.Drawing.Size(136, 23);
            this.textBoxNbParticipantSession.TabIndex = 7;
            // 
            // textBoxDateSession
            // 
            this.textBoxDateSession.Location = new System.Drawing.Point(190, 31);
            this.textBoxDateSession.Margin = new System.Windows.Forms.Padding(5);
            this.textBoxDateSession.Name = "textBoxDateSession";
            this.textBoxDateSession.Size = new System.Drawing.Size(136, 23);
            this.textBoxDateSession.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(364, 86);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 15);
            this.label10.TabIndex = 4;
            this.label10.Text = "Coût ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(364, 38);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(33, 15);
            this.label9.TabIndex = 3;
            this.label9.Text = "Lieu";
            // 
            // comboBoxListeActivite
            // 
            this.comboBoxListeActivite.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxListeActivite.FormattingEnabled = true;
            this.comboBoxListeActivite.ItemHeight = 15;
            this.comboBoxListeActivite.Location = new System.Drawing.Point(459, 30);
            this.comboBoxListeActivite.Margin = new System.Windows.Forms.Padding(5);
            this.comboBoxListeActivite.Name = "comboBoxListeActivite";
            this.comboBoxListeActivite.Size = new System.Drawing.Size(304, 23);
            this.comboBoxListeActivite.TabIndex = 4;
            this.comboBoxListeActivite.SelectedIndexChanged += new System.EventHandler(this.ComboBoxListeActivite_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridViewSessionsDeFormation);
            this.groupBox3.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(38, 494);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox3.Size = new System.Drawing.Size(1054, 234);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Liste des sessions de formation";
            // 
            // dataGridViewSessionsDeFormation
            // 
            this.dataGridViewSessionsDeFormation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewSessionsDeFormation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewSessionsDeFormation.Location = new System.Drawing.Point(42, 48);
            this.dataGridViewSessionsDeFormation.Name = "dataGridViewSessionsDeFormation";
            this.dataGridViewSessionsDeFormation.RowTemplate.Height = 24;
            this.dataGridViewSessionsDeFormation.Size = new System.Drawing.Size(954, 156);
            this.dataGridViewSessionsDeFormation.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(892, 1049);
            this.button1.Margin = new System.Windows.Forms.Padding(5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(305, 66);
            this.button1.TabIndex = 9;
            this.button1.Text = "Supprimer une session de formation";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(892, 942);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(304, 57);
            this.button2.TabIndex = 10;
            this.button2.Text = "Supprime une session de formation";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.comboBoxListeActivite);
            this.groupBox4.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(38, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1054, 80);
            this.groupBox4.TabIndex = 11;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Activités";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sélectionnez une activitée";
            // 
            // FormGestionSessionDeFormation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1127, 765);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormGestionSessionDeFormation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GESTION DES SESSIONS DE FORMATION";
            this.Load += new System.EventHandler(this.FormGestionSessionDeFormation_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSessionsDeFormation)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxDureeAction;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxCoutAction;
        private System.Windows.Forms.TextBox textBoxDateCreationAction;
        private System.Windows.Forms.TextBox textBoxCodeAction;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxListeAction;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxListeActivite;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBoxCoutHebergementSession;
        private System.Windows.Forms.TextBox textBoxNbParticipantSession;
        private System.Windows.Forms.TextBox textBoxDateSession;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonAjouterSession;
        private System.Windows.Forms.ComboBox comboBoxListeLieu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridViewSessionsDeFormation;
    }
}