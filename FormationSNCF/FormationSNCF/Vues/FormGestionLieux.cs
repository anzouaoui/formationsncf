﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionLieu : Form
    {
        public FormGestionLieu()
        {
            InitializeComponent();
        }

        private void FormGestionLieu_Load(object sender, EventArgs e)
        {
            dataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
        }

        private void ButtonAjoutLieu_Click(object sender, EventArgs e)
        {
            if (textBoxLiblle.Text.Length < 3)
            {
                MessageBox.Show("Le lieu doit etre compose d'au moins 3 caracteres");
                textBoxLiblle.Text = "";
                textBoxLiblle.Focus();
            }
            else if (textBoxCodePostal.Text.Length == 0)
            {
                MessageBox.Show("Le code postal est obligatoire");
                textBoxCodePostal.Focus();
            }
            else if (textBoxTelephone.Text.Length == 0)
            {
                MessageBox.Show("Le numero de telephone est obligatoire");
                textBoxTelephone.Focus();
            }
            else
            {
                dataGridViewListeLieu.DataSource = null;
                int numeroMax = 0;
                foreach (Lieu lieuCourant in Donnees.CollectionLieu)
                {
                    if(lieuCourant.Numero > numeroMax)
                    {
                        numeroMax = lieuCourant.Numero;
                    }
                }
                Lieu unLieu = new Lieu(numeroMax + 1, textBoxLiblle.Text, textBoxCodePostal.Text, textBoxTelephone.Text);
                Donnees.CollectionLieu.Add(unLieu);
                dataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
            }
        }

        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (textBoxCodePostal.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodePostal.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffre");
                    textBoxCodePostal.Text = "";
                    textBoxCodePostal.Focus();
                }
            }
        }

        private void TextBoxTelephone_Leave(object sender, EventArgs e)
        {
            if (textBoxTelephone.Text != "")
            {
                if (!Formulaire.VerificationTelephone(textBoxTelephone.Text))
                {
                    MessageBox.Show("Saisir un numero de telephone a 10 chiffre");
                    textBoxTelephone.Text = "";
                    textBoxTelephone.Focus();
                }
            }
        }

        private void textBoxCodePostal_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
