﻿namespace FormationSNCF.Vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.lieuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesLieuxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aGENTToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AjouterAgent = new System.Windows.Forms.ToolStripMenuItem();
            this.AfficherAgent = new System.Windows.Forms.ToolStripMenuItem();
            this.fORMATIONSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesFormationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aCTIVITESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GestionDesActionsDeFormationsToolsTripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionSessionDeFormationsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lieuToolStripMenuItem,
            this.aGENTToolStripMenuItem,
            this.fORMATIONSToolStripMenuItem,
            this.aCTIVITESToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(422, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // lieuToolStripMenuItem
            // 
            this.lieuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesLieuxToolStripMenuItem});
            this.lieuToolStripMenuItem.Name = "lieuToolStripMenuItem";
            this.lieuToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.lieuToolStripMenuItem.Text = "LIEUX";
            // 
            // gestionDesLieuxToolStripMenuItem
            // 
            this.gestionDesLieuxToolStripMenuItem.Name = "gestionDesLieuxToolStripMenuItem";
            this.gestionDesLieuxToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.gestionDesLieuxToolStripMenuItem.Text = "GESTION DES LIEUX";
            this.gestionDesLieuxToolStripMenuItem.Click += new System.EventHandler(this.GestionDesLieuxToolStripMenuItem_Click);
            // 
            // aGENTToolStripMenuItem
            // 
            this.aGENTToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AjouterAgent,
            this.AfficherAgent});
            this.aGENTToolStripMenuItem.Name = "aGENTToolStripMenuItem";
            this.aGENTToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.aGENTToolStripMenuItem.Text = "AGENT";
            // 
            // AjouterAgent
            // 
            this.AjouterAgent.Name = "AjouterAgent";
            this.AjouterAgent.Size = new System.Drawing.Size(192, 22);
            this.AjouterAgent.Text = "GESTION DES AGENTS";
            this.AjouterAgent.Click += new System.EventHandler(this.AjouterAgentToolStripMenuItem_Click);
            // 
            // AfficherAgent
            // 
            this.AfficherAgent.Name = "AfficherAgent";
            this.AfficherAgent.Size = new System.Drawing.Size(192, 22);
            this.AfficherAgent.Text = "AFFICHAGE AGENTS";
            this.AfficherAgent.Click += new System.EventHandler(this.AfficherAgentToolStripMenuItem_Click);
            // 
            // fORMATIONSToolStripMenuItem
            // 
            this.fORMATIONSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesFormationsToolStripMenuItem,
            this.gestionSessionDeFormationsToolStripMenuItem});
            this.fORMATIONSToolStripMenuItem.Name = "fORMATIONSToolStripMenuItem";
            this.fORMATIONSToolStripMenuItem.Size = new System.Drawing.Size(94, 20);
            this.fORMATIONSToolStripMenuItem.Text = "FORMATIONS";
            // 
            // gestionDesFormationsToolStripMenuItem
            // 
            this.gestionDesFormationsToolStripMenuItem.Name = "gestionDesFormationsToolStripMenuItem";
            this.gestionDesFormationsToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.gestionDesFormationsToolStripMenuItem.Text = "Gestion des formations";
            this.gestionDesFormationsToolStripMenuItem.Click += new System.EventHandler(this.GestionDesActionsDeFormationsToolsTripMenuItem_Click);
            // 
            // aCTIVITESToolStripMenuItem
            // 
            this.aCTIVITESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GestionDesActionsDeFormationsToolsTripMenuItem});
            this.aCTIVITESToolStripMenuItem.Name = "aCTIVITESToolStripMenuItem";
            this.aCTIVITESToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.aCTIVITESToolStripMenuItem.Text = "ACTIVITES";
            // 
            // GestionDesActionsDeFormationsToolsTripMenuItem
            // 
            this.GestionDesActionsDeFormationsToolsTripMenuItem.Name = "GestionDesActionsDeFormationsToolsTripMenuItem";
            this.GestionDesActionsDeFormationsToolsTripMenuItem.Size = new System.Drawing.Size(203, 22);
            this.GestionDesActionsDeFormationsToolsTripMenuItem.Text = "GESTION DES ACTIVITES";
            this.GestionDesActionsDeFormationsToolsTripMenuItem.Click += new System.EventHandler(this.GestionDesActivitesToolsTripMenuItem_Click);
            // 
            // gestionSessionDeFormationsToolStripMenuItem
            // 
            this.gestionSessionDeFormationsToolStripMenuItem.Name = "gestionSessionDeFormationsToolStripMenuItem";
            this.gestionSessionDeFormationsToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.gestionSessionDeFormationsToolStripMenuItem.Text = "Session de formations";
            this.gestionSessionDeFormationsToolStripMenuItem.Click += new System.EventHandler(this.GestionDesSessionsDeFormationsToolStripMenuItem_Click);
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = global::FormationSNCF.Properties.Resources.logo;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(422, 332);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFenetrePrincipale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormationSNCF";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormFenetrePrincipale_Closing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem lieuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesLieuxToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aGENTToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AjouterAgent;
        private System.Windows.Forms.ToolStripMenuItem fORMATIONSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesFormationsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aCTIVITESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem GestionDesActionsDeFormationsToolsTripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AfficherAgent;
        private System.Windows.Forms.ToolStripMenuItem gestionSessionDeFormationsToolStripMenuItem;
    }
}