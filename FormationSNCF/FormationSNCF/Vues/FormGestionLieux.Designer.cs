﻿namespace FormationSNCF.Vues
{
    partial class FormGestionLieu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxAjoutLieu = new System.Windows.Forms.GroupBox();
            this.buttonAjouter = new System.Windows.Forms.Button();
            this.textBoxTelephone = new System.Windows.Forms.TextBox();
            this.textBoxCodePostal = new System.Windows.Forms.TextBox();
            this.textBoxLiblle = new System.Windows.Forms.TextBox();
            this.labelTelephone = new System.Windows.Forms.Label();
            this.labelCodePostal = new System.Windows.Forms.Label();
            this.labelLibelle = new System.Windows.Forms.Label();
            this.groupBoxListeLieu = new System.Windows.Forms.GroupBox();
            this.dataGridViewListeLieu = new System.Windows.Forms.DataGridView();
            this.groupBoxAjoutLieu.SuspendLayout();
            this.groupBoxListeLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeLieu)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxAjoutLieu
            // 
            this.groupBoxAjoutLieu.Controls.Add(this.buttonAjouter);
            this.groupBoxAjoutLieu.Controls.Add(this.textBoxTelephone);
            this.groupBoxAjoutLieu.Controls.Add(this.textBoxCodePostal);
            this.groupBoxAjoutLieu.Controls.Add(this.textBoxLiblle);
            this.groupBoxAjoutLieu.Controls.Add(this.labelTelephone);
            this.groupBoxAjoutLieu.Controls.Add(this.labelCodePostal);
            this.groupBoxAjoutLieu.Controls.Add(this.labelLibelle);
            this.groupBoxAjoutLieu.Location = new System.Drawing.Point(13, 13);
            this.groupBoxAjoutLieu.Name = "groupBoxAjoutLieu";
            this.groupBoxAjoutLieu.Size = new System.Drawing.Size(400, 255);
            this.groupBoxAjoutLieu.TabIndex = 0;
            this.groupBoxAjoutLieu.TabStop = false;
            this.groupBoxAjoutLieu.Text = "Ajout d\'un lieu";
            // 
            // buttonAjouter
            // 
            this.buttonAjouter.Location = new System.Drawing.Point(241, 191);
            this.buttonAjouter.Name = "buttonAjouter";
            this.buttonAjouter.Size = new System.Drawing.Size(75, 23);
            this.buttonAjouter.TabIndex = 6;
            this.buttonAjouter.Text = "Ajouter";
            this.buttonAjouter.UseVisualStyleBackColor = true;
            this.buttonAjouter.Click += new System.EventHandler(this.ButtonAjoutLieu_Click);
            // 
            // textBoxTelephone
            // 
            this.textBoxTelephone.Location = new System.Drawing.Point(112, 120);
            this.textBoxTelephone.Name = "textBoxTelephone";
            this.textBoxTelephone.Size = new System.Drawing.Size(205, 20);
            this.textBoxTelephone.TabIndex = 5;
            this.textBoxTelephone.Leave += new System.EventHandler(this.TextBoxTelephone_Leave);
            // 
            // textBoxCodePostal
            // 
            this.textBoxCodePostal.Location = new System.Drawing.Point(112, 72);
            this.textBoxCodePostal.Name = "textBoxCodePostal";
            this.textBoxCodePostal.Size = new System.Drawing.Size(205, 20);
            this.textBoxCodePostal.TabIndex = 4;
            this.textBoxCodePostal.TextChanged += new System.EventHandler(this.textBoxCodePostal_TextChanged);
            this.textBoxCodePostal.Leave += new System.EventHandler(this.TextBoxCodePostal_Leave);
            // 
            // textBoxLiblle
            // 
            this.textBoxLiblle.Location = new System.Drawing.Point(112, 30);
            this.textBoxLiblle.Name = "textBoxLiblle";
            this.textBoxLiblle.Size = new System.Drawing.Size(205, 20);
            this.textBoxLiblle.TabIndex = 3;
            // 
            // labelTelephone
            // 
            this.labelTelephone.AutoSize = true;
            this.labelTelephone.Location = new System.Drawing.Point(36, 123);
            this.labelTelephone.Name = "labelTelephone";
            this.labelTelephone.Size = new System.Drawing.Size(58, 13);
            this.labelTelephone.TabIndex = 2;
            this.labelTelephone.Text = "Telephone";
            // 
            // labelCodePostal
            // 
            this.labelCodePostal.AutoSize = true;
            this.labelCodePostal.Location = new System.Drawing.Point(36, 72);
            this.labelCodePostal.Name = "labelCodePostal";
            this.labelCodePostal.Size = new System.Drawing.Size(64, 13);
            this.labelCodePostal.TabIndex = 1;
            this.labelCodePostal.Text = "Code Postal";
            // 
            // labelLibelle
            // 
            this.labelLibelle.AutoSize = true;
            this.labelLibelle.Location = new System.Drawing.Point(36, 38);
            this.labelLibelle.Name = "labelLibelle";
            this.labelLibelle.Size = new System.Drawing.Size(37, 13);
            this.labelLibelle.TabIndex = 0;
            this.labelLibelle.Text = "Libelle";
            // 
            // groupBoxListeLieu
            // 
            this.groupBoxListeLieu.Controls.Add(this.dataGridViewListeLieu);
            this.groupBoxListeLieu.Location = new System.Drawing.Point(13, 274);
            this.groupBoxListeLieu.Name = "groupBoxListeLieu";
            this.groupBoxListeLieu.Size = new System.Drawing.Size(400, 202);
            this.groupBoxListeLieu.TabIndex = 1;
            this.groupBoxListeLieu.TabStop = false;
            this.groupBoxListeLieu.Text = "Liste des lieux";
            // 
            // dataGridViewListeLieu
            // 
            this.dataGridViewListeLieu.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeLieu.Location = new System.Drawing.Point(6, 19);
            this.dataGridViewListeLieu.Name = "dataGridViewListeLieu";
            this.dataGridViewListeLieu.Size = new System.Drawing.Size(388, 177);
            this.dataGridViewListeLieu.TabIndex = 0;
            // 
            // FormGestionLieu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(425, 488);
            this.Controls.Add(this.groupBoxListeLieu);
            this.Controls.Add(this.groupBoxAjoutLieu);
            this.Name = "FormGestionLieu";
            this.Text = "GESTION DES LIEUX";
            this.Load += new System.EventHandler(this.FormGestionLieu_Load);
            this.groupBoxAjoutLieu.ResumeLayout(false);
            this.groupBoxAjoutLieu.PerformLayout();
            this.groupBoxListeLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeLieu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxAjoutLieu;
        private System.Windows.Forms.Label labelTelephone;
        private System.Windows.Forms.Label labelCodePostal;
        private System.Windows.Forms.Label labelLibelle;
        private System.Windows.Forms.GroupBox groupBoxListeLieu;
        private System.Windows.Forms.Button buttonAjouter;
        private System.Windows.Forms.TextBox textBoxTelephone;
        private System.Windows.Forms.TextBox textBoxCodePostal;
        private System.Windows.Forms.TextBox textBoxLiblle;
        private System.Windows.Forms.DataGridView dataGridViewListeLieu;
    }
}