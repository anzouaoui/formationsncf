﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Vues;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    /// <summary>
    /// Interface qui permet la gestion des actions de formations
    /// </summary>
    public partial class FormGestionActionFormation : Form
    {
        public FormGestionActionFormation()
        {
            InitializeComponent();
        }
      
        private void TextBoxDateCreation_Leave(object sender, EventArgs e)
        {
            if (textBoxDateCreation.Text != "")
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateCreation.Text))
                {
                    MessageBox.Show("Veuillez saisir une date au format jj-mm-aaaa");
                    textBoxDateCreation.Clear();
                    textBoxDateCreation.Focus();
                }
            }
        }

        private void TextBoxDureeAction_Leave(object sender, EventArgs e)
        {
            if (textBoxDureeAction.Text != "")
            {
                if (!Numerique.IsNumerique(textBoxDureeAction.Text))
                {
                    MessageBox.Show("Veuillez saisir une valeur entière");
                    textBoxDureeAction.Clear();
                    textBoxDureeAction.Focus();
                }
            }
        }

        private void FormGestionActionFormation_Load(object sender, EventArgs e)
        {
            comboBoxListeActivite.DataSource = Donnees.CollectionActivite;
            comboBoxListeActivite.DisplayMember = "LibelleActivite";
        }

        private void ComboxListeActivite_SelectedIndexChanged(object sender, EventArgs e)
        {
            Activite activiteSelectionnee = comboBoxListeActivite.SelectedItem as Activite;
            dataGridViewActionDeFormation.DataSource = activiteSelectionnee.ActionFormation;
        }

        private void ButtonAjoutActionFormation_Click(object sender, EventArgs e)
        {
            if (comboBoxListeActivite.SelectedIndex == -1)
            {
                MessageBox.Show("Selectionnez une activite de formation");
            }
            else if (textBoxIntituleActionFormation.Text == "")
            {
                MessageBox.Show("Saisir l'intitule de l'action de formation");
            }
            else if (textBoxCodeAction.Text == "")
            {
                MessageBox.Show("Saisir le code le l'action de formation");
            }
            else if (textBoxCoutAction.Text == "") 
            {
                MessageBox.Show("Saisir le code l'action");
            }
            else if (textBoxDureeAction.Text == "")
            {
                MessageBox.Show("Saisir la duree de l'action de formation");
            }
            else
            {
                Activite activiteSelectionnee = comboBoxListeActivite.SelectedItem as Activite;
                if (activiteSelectionnee != null)
                {
                    activiteSelectionnee.AjouterActionFormation(textBoxCodeAction.Text, textBoxIntituleActionFormation.Text, decimal.Parse(textBoxCoutAction.Text), int.Parse(textBoxDureeAction.Text), DateTime.Parse(textBoxDateCreation.Text));
                }
                dataGridViewActionDeFormation.DataSource=activiteSelectionnee.ActionFormation;

                textBoxIntituleActionFormation.Clear();
                textBoxCodeAction.Clear();
                textBoxCoutAction.Clear();
                textBoxDureeAction.Clear();
                textBoxDateCreation.Clear();
            }
        }      
        

    }
}
