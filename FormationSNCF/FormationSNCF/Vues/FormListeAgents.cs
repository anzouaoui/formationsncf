﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;


namespace FormationSNCF.Vues
{
    public partial class FormListeAgent : Form
    {
        private Agent _agentSelectionne;

        public FormListeAgent()
        {
            InitializeComponent();
        }

        private void ComboBoxLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxLieu.Text != "Sélectionner")
            {
                if (_agentSelectionne != null)
                {
                    _agentSelectionne.LieuDeTravaille = comboBoxLieu.SelectedItem as Lieu;
                }
            }
        }

        private void TextBoxDateNaiss_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
            {
                if (textBoxDateNaissance.Text != "")
                {
                    if (!Formulaire.VerificationFormatDate(textBoxDateNaissance.Text))
                    {
                        MessageBox.Show("Le format est incorrect");
                        textBoxDateNaissance.Focus();
                    }
                    else
                    {
                        _agentSelectionne.DateNaissance = DateTime.Parse(textBoxDateNaissance.Text);
                    }
                }
            }
        }

        private void TextBoxDateEmbauche_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
            {
                if (textBoxDateEmbauche.Text != "")
                {
                    if (!Formulaire.VerificationFormatDate(textBoxDateEmbauche.Text))
                    {
                        MessageBox.Show("Le format est incorrect");
                        this.textBoxDateEmbauche.Focus();
                    }
                    else
                    {
                        _agentSelectionne.DateEmbauche = DateTime.Parse(textBoxDateEmbauche.Text);
                    }
                }
            }
        }

        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodePostal.Text))
                {
                    MessageBox.Show("Le format est incorrect:");
                    textBoxCodePostal.Text = "";
                    this.textBoxCodePostal.Focus();
                }
            }
        }

        private void ComboBoxCivilite_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxCivilite.Text != "Sélectionner")
                if (_agentSelectionne != null)
                    _agentSelectionne.Civilite = comboBoxCivilite.Text;

        }

        private void TextBoxAdresse1_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
                _agentSelectionne.Adresse1 = textBoxAdresse1.Text;
        }

        private void TextBoxAdresse2_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
                _agentSelectionne.Adresse2 = textBoxAdresse2.Text;
        }

        private void TextBoxVille_Leave(object sender, EventArgs e)
        {
            if (_agentSelectionne != null)
                _agentSelectionne.Ville = textBoxVille.Text;
        }

        
        private void FormListeAgent_Load(object sender, EventArgs e)
        {
            comboBoxLieu.DataSource = Donnees.CollectionLieu;
            comboBoxLieu.DisplayMember = "Libelle";
            dataGridViewListeAgents.DataSource = Donnees.CollectionAgent;
            for (int i = 0; i < dataGridViewListeAgents.Columns.Count; i++)
            {
                if (i == 0 || i > 3)
                    dataGridViewListeAgents.Columns[i].Visible = false;
            }

        }

        private void DataGridViewListeAgents_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewListeAgents.SelectedRows.Count > 0)
            {
                _agentSelectionne = dataGridViewListeAgents.SelectedRows[0].DataBoundItem as Agent;
                textBoxVille.Text = _agentSelectionne.Ville;
                textBoxAdresse1.Text = _agentSelectionne.Adresse1;
                textBoxAdresse2.Text = _agentSelectionne.Adresse2;
                comboBoxCivilite.Text = _agentSelectionne.Civilite;
                textBoxCodePostal.Text = _agentSelectionne.CodePostal;
                textBoxDateEmbauche.Text = Convert.ToString(_agentSelectionne.DateEmbauche);
                textBoxDateNaissance.Text = Convert.ToString(_agentSelectionne.DateNaissance);
                comboBoxLieu.Text = Convert.ToString(_agentSelectionne.LieuDeTravaille);
                
            }
        }

        private void textBoxCodePostal_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
