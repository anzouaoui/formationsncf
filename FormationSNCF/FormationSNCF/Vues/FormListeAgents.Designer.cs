﻿namespace FormationSNCF.Vues
{
    partial class FormListeAgent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridViewListeAgents = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBoxLieu = new System.Windows.Forms.ComboBox();
            this.comboBoxCivilite = new System.Windows.Forms.ComboBox();
            this.textBoxDateEmbauche = new System.Windows.Forms.TextBox();
            this.textBoxDateNaissance = new System.Windows.Forms.TextBox();
            this.textBoxVille = new System.Windows.Forms.TextBox();
            this.textBoxCodePostal = new System.Windows.Forms.TextBox();
            this.textBoxAdresse2 = new System.Windows.Forms.TextBox();
            this.textBoxAdresse1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeAgents)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridViewListeAgents);
            this.groupBox1.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(34, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(562, 417);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Liste des agents";
            // 
            // dataGridViewListeAgents
            // 
            this.dataGridViewListeAgents.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewListeAgents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewListeAgents.Location = new System.Drawing.Point(31, 41);
            this.dataGridViewListeAgents.MultiSelect = false;
            this.dataGridViewListeAgents.Name = "dataGridViewListeAgents";
            this.dataGridViewListeAgents.RowTemplate.Height = 24;
            this.dataGridViewListeAgents.Size = new System.Drawing.Size(495, 331);
            this.dataGridViewListeAgents.TabIndex = 2;
            this.dataGridViewListeAgents.RowEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewListeAgents_RowEnter);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboBoxLieu);
            this.groupBox2.Controls.Add(this.comboBoxCivilite);
            this.groupBox2.Controls.Add(this.textBoxDateEmbauche);
            this.groupBox2.Controls.Add(this.textBoxDateNaissance);
            this.groupBox2.Controls.Add(this.textBoxVille);
            this.groupBox2.Controls.Add(this.textBoxCodePostal);
            this.groupBox2.Controls.Add(this.textBoxAdresse2);
            this.groupBox2.Controls.Add(this.textBoxAdresse1);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(631, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(538, 417);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Agent séléctionné";
            // 
            // comboBoxLieu
            // 
            this.comboBoxLieu.FormattingEnabled = true;
            this.comboBoxLieu.ItemHeight = 15;
            this.comboBoxLieu.Location = new System.Drawing.Point(254, 352);
            this.comboBoxLieu.Name = "comboBoxLieu";
            this.comboBoxLieu.Size = new System.Drawing.Size(238, 23);
            this.comboBoxLieu.TabIndex = 12;
            this.comboBoxLieu.SelectedIndexChanged += new System.EventHandler(this.ComboBoxLieu_SelectedIndexChanged);
            // 
            // comboBoxCivilite
            // 
            this.comboBoxCivilite.FormattingEnabled = true;
            this.comboBoxCivilite.ItemHeight = 15;
            this.comboBoxCivilite.Items.AddRange(new object[] {
            "Sélectionner",
            "M.",
            "Mme.",
            "Mlle."});
            this.comboBoxCivilite.Location = new System.Drawing.Point(254, 41);
            this.comboBoxCivilite.Name = "comboBoxCivilite";
            this.comboBoxCivilite.Size = new System.Drawing.Size(238, 23);
            this.comboBoxCivilite.TabIndex = 3;
            this.comboBoxCivilite.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCivilite_SelectedIndexChanged);
            // 
            // textBoxDateEmbauche
            // 
            this.textBoxDateEmbauche.Location = new System.Drawing.Point(254, 299);
            this.textBoxDateEmbauche.Name = "textBoxDateEmbauche";
            this.textBoxDateEmbauche.Size = new System.Drawing.Size(140, 23);
            this.textBoxDateEmbauche.TabIndex = 11;
            this.textBoxDateEmbauche.Leave += new System.EventHandler(this.TextBoxDateEmbauche_Leave);
            // 
            // textBoxDateNaissance
            // 
            this.textBoxDateNaissance.Location = new System.Drawing.Point(254, 260);
            this.textBoxDateNaissance.Name = "textBoxDateNaissance";
            this.textBoxDateNaissance.Size = new System.Drawing.Size(140, 23);
            this.textBoxDateNaissance.TabIndex = 10;
            this.textBoxDateNaissance.Leave += new System.EventHandler(this.TextBoxDateNaiss_Leave);
            // 
            // textBoxVille
            // 
            this.textBoxVille.Location = new System.Drawing.Point(254, 208);
            this.textBoxVille.Name = "textBoxVille";
            this.textBoxVille.Size = new System.Drawing.Size(238, 23);
            this.textBoxVille.TabIndex = 9;
            this.textBoxVille.Leave += new System.EventHandler(this.TextBoxVille_Leave);
            // 
            // textBoxCodePostal
            // 
            this.textBoxCodePostal.Location = new System.Drawing.Point(254, 179);
            this.textBoxCodePostal.Name = "textBoxCodePostal";
            this.textBoxCodePostal.Size = new System.Drawing.Size(238, 23);
            this.textBoxCodePostal.TabIndex = 8;
            this.textBoxCodePostal.TextChanged += new System.EventHandler(this.textBoxCodePostal_TextChanged);
            this.textBoxCodePostal.Leave += new System.EventHandler(this.TextBoxCodePostal_Leave);
            // 
            // textBoxAdresse2
            // 
            this.textBoxAdresse2.Location = new System.Drawing.Point(254, 131);
            this.textBoxAdresse2.Name = "textBoxAdresse2";
            this.textBoxAdresse2.Size = new System.Drawing.Size(238, 23);
            this.textBoxAdresse2.TabIndex = 7;
            this.textBoxAdresse2.Text = " ";
            this.textBoxAdresse2.Leave += new System.EventHandler(this.TextBoxAdresse2_Leave);
            // 
            // textBoxAdresse1
            // 
            this.textBoxAdresse1.Location = new System.Drawing.Point(254, 102);
            this.textBoxAdresse1.Name = "textBoxAdresse1";
            this.textBoxAdresse1.Size = new System.Drawing.Size(238, 23);
            this.textBoxAdresse1.TabIndex = 6;
            this.textBoxAdresse1.Leave += new System.EventHandler(this.TextBoxAdresse1_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 352);
            this.label11.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(141, 15);
            this.label11.TabIndex = 33;
            this.label11.Text = "Lieu de rattachement";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(36, 302);
            this.label10.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(119, 15);
            this.label10.TabIndex = 32;
            this.label10.Text = "Date D\'embauche";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(36, 263);
            this.label9.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 15);
            this.label9.TabIndex = 31;
            this.label9.Text = "Date de naissance";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(36, 216);
            this.label8.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 15);
            this.label8.TabIndex = 30;
            this.label8.Text = "Ville";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(36, 179);
            this.label7.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 15);
            this.label7.TabIndex = 29;
            this.label7.Text = "Code Postal";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(36, 131);
            this.label6.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 15);
            this.label6.TabIndex = 28;
            this.label6.Text = "Adresse 2";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(36, 102);
            this.label5.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 15);
            this.label5.TabIndex = 27;
            this.label5.Text = "Adresse 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 41);
            this.label2.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 24;
            this.label2.Text = "Civilité";
            // 
            // FormListeAgent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1181, 497);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Lucida Bright", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1406, 801);
            this.MinimizeBox = false;
            this.Name = "FormListeAgent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Liste des agents";
            this.Load += new System.EventHandler(this.FormListeAgent_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewListeAgents)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxLieu;
        private System.Windows.Forms.ComboBox comboBoxCivilite;
        private System.Windows.Forms.TextBox textBoxDateEmbauche;
        private System.Windows.Forms.TextBox textBoxDateNaissance;
        private System.Windows.Forms.TextBox textBoxVille;
        private System.Windows.Forms.TextBox textBoxCodePostal;
        private System.Windows.Forms.TextBox textBoxAdresse2;
        private System.Windows.Forms.TextBox textBoxAdresse1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewListeAgents;
    }
}