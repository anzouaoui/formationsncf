﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using FormationSNCF.Vues;




namespace FormationSNCF
{
    static class Program
    {
        /// <summary>
        /// Point d'entrée principal de l'application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormFenetrePrincipale());
        }


    }

    //class MaFenetre : Form
    //{
    //    //déclaration d'un bouton
    //    private System.Windows.Forms.Button monBouton;

    //    public MaFenetre()
    //    {
    //        //Initialisation d'un bouton
    //        monBouton = new Button();

    //        //Les objets de type "Button" présentent l'événement CLICK
    //        //Inscription à l'événement this.monBouton.Click += new System.EventHandler(UtilisationEvenement);
    //        this.Controls.Add(monBouton);
    //    }

    //    //Méthode appelée lors du click sur le bouton
    //    public void UtilisationEvenement(object sender, EventArgs e)
    //    {
    //        MessageBox.Show("J'ai cliqué sur un bouton !!!");
    //    }
    //}


}
